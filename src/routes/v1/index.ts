import mangaRouter from "./manga";
import { Router } from "express";

const router = Router();

router.use("/komik", mangaRouter);

export default router;
