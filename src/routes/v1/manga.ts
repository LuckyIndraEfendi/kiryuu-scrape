import { Router } from "express";
import { getManga, getMangaDetail } from "../../controllers/v1/manga";

const router = Router();

router.get("/manga", getManga);
router.get("/manga/:id", getMangaDetail);

export default router;
