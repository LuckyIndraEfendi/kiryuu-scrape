import * as cheerio from "cheerio";
import axios from "axios";

export const getManga = async (req: any, res: any) => {
  // const {ongoing,completed,hiatus,all} = req.query.status
  // const {all,manga,manhwa,manhua,comic,novel} = req.query.type
  // const {default,a-z,z-a,update,latest,popular} = req.query.orderBy
  try {
    const url = await axios.get(`https://kiryuu.id/manga/`, {
      params: {
        page: req.query.page,
        status: req.query.status,
        type: req.query.type,
        order: req.query.orderBy,
      },
    });
    const $ = cheerio.load(url.data);
    const manga: any = [];
    $(
      "#content > div.wrapper > div.postbody > div.bixbox.seriesearch > div.mrgn > div.listupd > div"
    ).each((i: number, el) => {
      manga.push({
        title: $(el)
          .find("div > a > div.bigor > div.tt")
          .text()
          .replace(/\n/g, "")
          .trim(),
        slug:
          $(el)
            .find("div > a")
            .attr("href")
            ?.replace("https://kiryuu.id/manga", "")
            ?.replace(/\/[^/]*$/, "") ?? "",
        banner: $(el).find("div > a > div.limit > img").attr("src"),
        chapter_title: $(el)
          .find("div > a > div.bigor > div.adds > div.epxs")
          .text(),
        rating: $(el)
          .find("div > a > div.bigor > div.adds > div.rt > div > div.numscore")
          .text(),
      });
    });

    const has_next = $(
      "#content > div.wrapper > div.postbody > div.bixbox.seriesearch > div.mrgn > div.hpage > a.r"
    ).text();
    const has_prev = $(
      "#content > div.wrapper > div.postbody > div.bixbox.seriesearch > div.mrgn > div.hpage > a.l"
    ).text();
    res.status(200).json({
      statusCode: 200,
      data: manga,
      has_next: !!has_next,
      has_prev: !!has_prev,
    });
  } catch (err: any) {
    res.status(500).json({
      statusCode: 500,
      message: err.message,
    });
  }
};

type genreType = {
  genre_title: string;
  genre_slug: string;
};

type chapterType = {
  chapter_title: string;
  chapter_release: string;
  chapter_slug: string;
};

export const getMangaDetail = async (req: any, res: any) => {
  try {
    const url = await axios.get(`https://kiryuu.id/manga/${req.params.id}`);
    const $ = cheerio.load(url.data);
    const title = $("#post-74957 > div.seriestucon > div.seriestuheader > h1")
      .text()
      .replace("Bahasa Indonesia", "");
    const synopsis = $(
      "#post-74957 > div.seriestucon > div.seriestucontent > div.seriestucontentr > div.seriestuhead > div.entry-content.entry-content-single > p"
    )
      .text()
      .replace(/\n/g, "")
      .trim();
    const followed = $(
      "#post-74957 > div.seriestucon > div.seriestucontent > div.seriestucontl > div.bmc"
    ).text();
    const banner = $(
      "#post-74957 > div.seriestucon > div.seriestucontent > div.seriestucontl > div.thumb > img"
    ).attr("src");
    const rating = $(
      "#post-74957 > div.seriestucon > div.seriestucontent > div.seriestucontl > div.rating.bixbox > div > div.num"
    ).text();

    const status = $(
      "#post-74957 > div.seriestucon > div.seriestucontent > div.seriestucontentr > div.seriestucont > div > table > tbody > tr:nth-child(1) > td:nth-child(2)"
    ).text();
    const released = $(
      "#post-74957 > div.seriestucon > div.seriestucontent > div.seriestucontentr > div.seriestucont > div > table > tbody > tr:nth-child(3) > td:nth-child(2)"
    ).text();
    const update_on = $(
      "#post-74957 > div.seriestucon > div.seriestucontent > div.seriestucontentr > div.seriestucont > div > table > tbody > tr:nth-child(9) > td:nth-child(2) > time"
    ).text();
    const type = $(
      "#post-74957 > div.seriestucon > div.seriestucontent > div.seriestucontentr > div.seriestucont > div > table > tbody > tr:nth-child(2) > td:nth-child(2)"
    ).text();
    const author = $(
      "#post-74957 > div.seriestucon > div.seriestucontent > div.seriestucontentr > div.seriestucont > div > table > tbody > tr:nth-child(4) > td:nth-child(2)"
    ).text();
    const serialization = $(
      "#post-74957 > div.seriestucon > div.seriestucontent > div.seriestucontentr > div.seriestucont > div > table > tbody > tr:nth-child(6) > td:nth-child(2)"
    ).text();
    const posted_on = $(
      "#post-74957 > div.seriestucon > div.seriestucontent > div.seriestucontentr > div.seriestucont > div > table > tbody > tr:nth-child(8) > td:nth-child(2) > time"
    ).text();
    const views = $(
      "#post-74957 > div.seriestucon > div.seriestucontent > div.seriestucontentr > div.seriestucont > div > table > tbody > tr:nth-child(10) > td:nth-child(2) > span"
    ).text();
    const genre: genreType[] = [];
    $(
      "#post-74957 > div.seriestucon > div.seriestucontent > div.seriestucontentr > div.seriestucont > div > div > a"
    ).each((i: number, el) => {
      genre.push({
        genre_title: $(el).text(),
        genre_slug:
          $(el)
            .attr("href")
            ?.replace("https://kiryuu.id/genres", "")
            ?.replace(/\/[^/]*$/, "") ?? "",
      });
    });

    const chapters: chapterType[] = [];

    $("#chapterlist > ul > li").each((i: number, el) => {
      chapters.push({
        chapter_title: $(el)
          .find("div > div.eph-num > a > span.chapternum")
          .text(),
        chapter_release: $(el)
          .find("div > div.eph-num > a > span.chapterdate")
          .text(),
        chapter_slug:
          $(el)
            .find("div > div.eph-num > a")
            .attr("href")
            ?.replace("https://kiryuu.id/", "") ?? "",
      });
    });
    res.status(200).json({
      statusCode: 200,
      data: {
        title,
        status,
        followed: parseInt(followed.replace(/\D/g, "")),
        type,
        synopsis,
        banner,
        rating,
        released,
        update_on,
        author,
        serialization,
        posted_on,
        views,
        genre,
        chapters,
      },
    });
  } catch (err: any) {
    res.status(500).json({
      statusCode: 500,
      message: err.message,
    });
  }
};
